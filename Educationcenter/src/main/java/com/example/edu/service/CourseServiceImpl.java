package com.example.edu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.edu.entity.Course;
import com.example.edu.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService{

	@Autowired
	private CourseRepository courseRepo;
	
	public List<Course> getCourses() {
		// TODO Auto-generated method stub
		return courseRepo.findAll();
	}

	
	public void save(Course course) {
		// TODO Auto-generated method stub
		courseRepo.save(course);
	}



	public List<Course> getCoursestByCategoryId(Integer catID) {
		
		return courseRepo.findByCategoryId(catID);
	}

}
