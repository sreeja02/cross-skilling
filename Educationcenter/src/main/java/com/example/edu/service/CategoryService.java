package com.example.edu.service;

import java.util.List;

import com.example.edu.entity.Category;

public interface CategoryService {
	public List<Category> getCategories();
	public void save(Category c);
}
