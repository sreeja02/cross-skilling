package com.example.edu.service;

import java.util.List;

import com.example.edu.entity.Course;

public interface CourseService {

	public List<Course> getCourses();

	public void save(Course course);
	
	public List<Course> getCoursestByCategoryId(Integer catID);
}
