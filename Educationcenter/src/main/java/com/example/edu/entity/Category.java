package com.example.edu.entity;

import java.util.List;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table
@Data
public class Category {

	@Id
	private Integer catid;
	private String categoryName;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="categoryId")
	public Set<Course> courses;
	
	
	public Category() {
	}
	public Category(Integer catid, String categoryName) {
		super();
		this.catid = catid;
		this.categoryName = categoryName;
	}
	
	
	
}
