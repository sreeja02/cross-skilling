package com.example.edu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.edu.entity.Category;
import com.example.edu.entity.Course;
import com.example.edu.service.CategoryService;
import com.example.edu.service.CourseService;

@Controller
@RequestMapping("api")
public class CourseController {

	@Autowired
	private CourseService courseService;
	@Autowired
	private CategoryService categoryService;

	
	@GetMapping("display")
	public ModelAndView displayCourses() {
		List<Course> courses = courseService.getCourses();
		System.out.println(courses);
		ModelAndView mv = new ModelAndView("Courses");
		mv.addObject("courses", courses);

		return mv;
	}

	@GetMapping("/showCourseForm")
	public ModelAndView showCourseForm() {
		ModelAndView mv = new ModelAndView("CoursesForm");
		mv.addObject("Course", new Course(0, "", 0.0f, 0l, 0));
		mv.addObject("categories",categoryService.getCategories());
		System.out.println(categoryService.getCategories());
		return mv;
	}

	@PostMapping("/courseSave")
	public ModelAndView saveOrUpdate(@ModelAttribute Course course) {
		System.out.println(course);
		courseService.save(course);
		ModelAndView modelView = new ModelAndView("redirect:/api/display");

		return modelView;
	}
	
	
	// Get Courses By Category
	  @GetMapping("/categoryCoures")  
		public ModelAndView  getCoursesByCategory( @RequestParam Integer categoryId ,Model model) {
			
			List<Course> courses = courseService.getCoursestByCategoryId(categoryId);
			System.out.println(courses);
			ModelAndView mv = new ModelAndView("Courses");
			mv.addObject("courses", courses);

			return mv;
	}
		  
	
	
	//---------------Category -------------------------------------------------------------
	
	@GetMapping("displayCategories")
	public ModelAndView displayCategories() {
		List<Category> categories = categoryService.getCategories();
		System.out.println(categories);
		ModelAndView mv = new ModelAndView("Categories");
		mv.addObject("categories", categories);

		return mv;
	}

	
	
	@GetMapping("/showCategoryForm")
	public ModelAndView showCategoryForm() {
		ModelAndView mv = new ModelAndView("CategoryForm");
		mv.addObject("category", new Category(0,""));
		return mv;
	}

	@PostMapping(path = "/categorySave")
	public ModelAndView saveOrUpdate(@ModelAttribute Category category) {
		System.out.println(category);
		categoryService.save(category);
		ModelAndView modelView = new ModelAndView("redirect:/api/displayCategories");

		return modelView;
	}
	
	//------------ Home---------------------------------------------------------
	
	@GetMapping("/home")
	public ModelAndView showHomePage() {
		ModelAndView mv = new ModelAndView("Home");
		return mv;
	}

	
	
	
}
