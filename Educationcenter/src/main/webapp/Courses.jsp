<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Courses</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
	crossorigin="anonymous">
	
</script>

<style>
body {
	margin-top: 2%;
}
</style>
</head>
<body>
	<div class="container">
		<a href="/api/showCourseForm" class="btn btn-primary"> Add Course </a>
		<a href="/api/displayCategories" class="btn btn-primary"> View Categories </a>
		<hr>
		<table class="table table-striped">
			<tr class="table-dark">
				<th>Course ID</th>
				<th>Name</th>
				<th>Fees</th>
				<th>Duration</th>
				<th>Category</th>
				<th>Action</th>
			</tr>
			<c:forEach var="c" items="${courses}">
				<tr>
					<td><c:out value="${c.getCourseId() }" /></td>
					<td><c:out value="${c.getCourseName() }" /></td>
					<td><c:out value="${c.getFees() }" /></td>
					<td><c:out value="${c.getDuration() }" /></td>
					<td><c:out value="${c.getCategory().getCategoryName() }" /></td>
					<td><a href="/api/update?courseId=${c.getCourseId()}" class="btn btn-sm btn-primary"> 
							update </a>
						<a href="/api/delete?courseId=${c.getCourseId()}" class="btn btn-sm btn-primary"> Delete </a>
				</tr>





			</c:forEach>

		</table>

	<a href="/api/home"> Back to Home Page </a>
	</div>
	
	
</body>
</html>