
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Courses</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
	crossorigin="anonymous">
	
</script>
</head>
<body style="margin-top:2%">
	<div class="container">
		<a href="/api/showCategoryForm" class="btn btn-primary"> Add
			Category </a>
			
		<a href="/api/display" class="btn btn-primary">View All Courses </a>
		<hr>
		<table class="table table-striped">
			<tr class="table-dark">
				<th>Category ID</th>
				<th>Category Name</th>
				
			</tr>
			<c:forEach var="c" items="${requestScope.categories}">
				<tr>
					<td><c:out value="${c.getCatid() }" /></td>
					<td><c:out value="${c.getCategoryName() }" /></td>
					<td><a href="/api/categoryCourse?categoryId=${c.getCatid()}" class="btn btn-sm btn-primary"> 
							View Courses </a>
			</tr>
			</c:forEach>
		</table>
		<a href="/api/home"> Back to Home Page </a>
		<hr>
	</div>
</body>
</html>