<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Course Details</title>
</head>
<body>
<div class="container">


<form:form  action ="/api/courseSave" method="post" modelAttribute="Course">
	<label>Course ID</label>
	<form:input path="courseId" />	
	<br>
	<label>CourseName</label>
	<form:input path="courseName" /><br><br>
	<label>Duration</label>
	<form:input path="duration" /><br><br>
	<label>Fees</label>
	<form:input path="fees" /><br><br>
	
	<br>
	<label>Category :</label>
	<form:select path="categoryId">
	<c:forEach var="c" items="${categories}">
			<form:option value="${c.getCatid() }"  label="${c.getCategoryName() }" />
			
	</c:forEach>
	</form:select>   
	<br><br>
	
	<input type="submit" value="Submit" class="btn btn-primary"/>
	<input type="reset" value="Reset" class="btn btn-primary"/>
</form:form>

<a href="/api/home"> Back to Home Page </a><hr>
</div>
</body>
</html>