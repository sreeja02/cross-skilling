<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Category Details</title>
</head>
<body>
<div class="container">


<form:form  action ="/api/categorySave" method="post" modelAttribute="category">
	<label>Category ID</label>
	<form:input path="catid" />
	
	
	<br>
	
	<label>Category Name</label>
		<form:input path="categoryName" /><br><br>
	
	
	<br>
	
	<input type="submit" value="Submit" class="btn btn-primary"/>
	<input type="reset" value="Reset" class="btn btn-primary"/>
</form:form>

<a href="/api/home"> Back to Home Page </a><hr>
</div>
</body>
</html>