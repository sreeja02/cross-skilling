package model;

public class User {
	private String fname;
	private String lname;
	private String DOB;
	private String gender;
	private String emailID;
	private String password;
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(String fname, String lname, String DOB, String gender, String emailID, String password) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.DOB = DOB;
		this.gender = gender;
		this.emailID = emailID;
		this.password = password;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String DOB) {
		this.DOB = DOB;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
