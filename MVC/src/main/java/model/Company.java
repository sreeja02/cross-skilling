package model;

public class Company {

	private int companyid;
	private String companyName;
	private float shareprice;
	
	public Company() {
		// TODO Auto-generated constructor stub
	}

	public Company(int companyid, String companyName, float shareprice) {
		super();
		this.companyid = companyid;
		this.companyName = companyName;
		this.shareprice = shareprice;
	}

	public int getCompanyid() {
		return companyid;
	}

	public void setCompanyid(int companyid) {
		this.companyid = companyid;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public float getShareprice() {
		return shareprice;
	}

	public void setShareprice(float shareprice) {
		this.shareprice = shareprice;
	}
	
}
