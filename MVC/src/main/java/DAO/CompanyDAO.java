package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import model.Company;

public class CompanyDAO {
	public List<Company> getCompanydetails() throws ClassNotFoundException, SQLException{
		List<Company> companyList = new LinkedList<Company>();
		
		Connection connect = DBConnection.getConnection();
		Statement stmt = connect.createStatement();
		
		ResultSet r = stmt.executeQuery("select * from company");
		while(r.next()) {
			companyList.add(new Company(r.getInt(1),r.getString(2),r.getFloat(3)));
		}
		
		return companyList;
	}

	public void updateSharePrice(Company c) throws ClassNotFoundException, SQLException {
		Connection connect = DBConnection.getConnection();
		PreparedStatement p = connect.prepareStatement("update company set shareprice=:1 where company_id = :2");
		
		p.setFloat(1,c.getShareprice());
		p.setInt(2, c.getCompanyid());
		p.executeUpdate();
	}
	
	public void deleteCompany(int companyId) throws ClassNotFoundException, SQLException {
		Connection connect = DBConnection.getConnection();
		
		PreparedStatement p = connect.prepareStatement("delete from company where company_id = :1");
		p.setInt(1, companyId);
		p.executeUpdate();
	}

				//							13
	public Company getCompanyDetailsById(int id) throws SQLException, ClassNotFoundException {
		Company companydetails  = null;
		Connection connect = DBConnection.getConnection();
		
		PreparedStatement p = connect.prepareStatement("select * from company where company_id = :1");
		p.setInt(1, id);
		
		ResultSet r = p.executeQuery();
		while(r.next()) {
			companydetails  = new Company(r.getInt(1),r.getString(2),r.getFloat(3));
		}
		
		return companydetails;
	}
	
	public int getMaxCompanyId() throws ClassNotFoundException, SQLException {
		Connection connect = DBConnection.getConnection();
		
		int companyId = 0;
		PreparedStatement p = connect.prepareStatement("select max(company_id) from company");
		ResultSet rs = p.executeQuery();
		
		if(rs.next())
			companyId = rs.getInt(1);
		
		return companyId;
	}

	public void save(Company company) throws SQLException, ClassNotFoundException {
		Connection connect = DBConnection.getConnection();
		PreparedStatement p = connect.prepareStatement("insert into company values(:1,:2,:3)");
		p.setInt(1, company.getCompanyid());
		p.setString(2, company.getCompanyName());
		p.setFloat(3, company.getShareprice());
		
		p.executeUpdate();
		
	}
	}

