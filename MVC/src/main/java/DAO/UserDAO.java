package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.User;

public class UserDAO {
	
	// user = 	Sreeja	Sravanam,	15-10-2022	female,	 sreeja.sravanam@concentrix.com,	Sreeja
	public void save(User user) throws ClassNotFoundException, SQLException {
		Connection connect = DBConnection.getConnection();
		
		PreparedStatement p = connect.prepareStatement("insert into sys.AccountUsers values(:1,:2,:3,:4,:5,:6)");
		p.setString(1, user.getFname());
		p.setString(2, user.getLname());
		p.setString(3, user.getDOB());
		p.setString(4, user.getGender());
		p.setString(5, user.getEmailID());
		p.setString(6, user.getPassword());
		
		p.executeUpdate();
	}

	public boolean isvalid(String email, String password) throws ClassNotFoundException, SQLException {
		Connection connect = DBConnection.getConnection();
		
		
		
		PreparedStatement ps = connect.prepareStatement("select * from accountusers where emailid=:1 and password = :2");
		ps.setString(1, email);
		ps.setString(2, password);
		
		ResultSet rs =  ps.executeQuery();
		boolean found = rs.next(); // true
		
		if(found)		
			return true;
		
		return false;
		
		
		
	}
	
}
