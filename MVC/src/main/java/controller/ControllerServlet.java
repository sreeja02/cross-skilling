package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.CompanyDAO;
import DAO.UserDAO;
import model.Company;
import model.User;

/**
 * Servlet implementation class ControllerServlet
 */
@WebServlet("/ControllerServlet")
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ControllerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("create") != null) {

			String fname = request.getParameter("fname"); // sreeja
			String lname = request.getParameter("lname"); // sravanam
			String date = request.getParameter("dob"); // 15-10-2022
			String gender = request.getParameter("gender"); // female
			String email = request.getParameter("email"); // sreeja.sravanam@concentrix.com
			String password = request.getParameter("psswd"); // sreeja

			// Sreeja Sravanam, 15-10-2022 female, sreeja.sravanam@concentrix.com, Sreeja
			User user = new User(fname, lname, date, gender, email, password);

			UserDAO userdao = new UserDAO();
			try {
				userdao.save(user);
				response.sendRedirect("Login.jsp");
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(request.getParameter("deleteid") != null) {
			try {
				deleteCompany(Integer.parseInt(request.getParameter("deleteid")),request,response);
			} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(request.getParameter("id") != null) {  // id = "13"
			try {
				showForm(Integer.parseInt(request.getParameter("id")),request,response);
			} catch (NumberFormatException | ClassNotFoundException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(request.getParameter("submit") != null) {
			
			int companyId = Integer.parseInt(request.getParameter("companyID"));
			String companyName = request.getParameter("companyName");
			float shareprice = Float.parseFloat(request.getParameter("shareprice"));
			
			saveorupdate(new Company(companyId,companyName , shareprice),request,response);
			
		}
		
		if(request.getParameter("list") != null) {
			try {
				getCompanyDetails(request, response);
			} catch (ClassNotFoundException | SQLException | ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		if (login != null) {
			String email = request.getParameter("email");
			String password = request.getParameter("pswd");
			try {
				validate(email, password, request, response);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private void validate(String email, String password, HttpServletRequest request, HttpServletResponse response)
			throws ClassNotFoundException, SQLException, ServletException, IOException {
		UserDAO u = new UserDAO();
		if (u.isvalid(email, password)) {
			getCompanyDetails(request, response);
		}

	}

	private void getCompanyDetails( HttpServletRequest request,HttpServletResponse response) throws ClassNotFoundException, SQLException, ServletException, IOException {

		CompanyDAO companyDao = new CompanyDAO();
		List<Company> companyList = companyDao.getCompanydetails();

		RequestDispatcher dispatcher = request.getRequestDispatcher("CompanyList.jsp");
		request.setAttribute("companyList", companyList);
		dispatcher.forward(request, response);
	}
	
	private void showForm(int id, HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException {
		CompanyDAO companyDao = new CompanyDAO ();
		
		Company  c = new Company(0,"",0.0f);
		
		if(id == 0) {	  // new company details		
			c .setCompanyid(companyDao.getMaxCompanyId()+1);
			c.setShareprice(0.0f);		
		}
		else { // updating the existing company
			c = companyDao.getCompanyDetailsById(id);   // c = companyDao.getCompanyDetailsById(13)
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("Company", c);
		
		response.sendRedirect("CompanyForm.jsp");
	}
	
	private void saveorupdate(Company company, HttpServletRequest request, HttpServletResponse response) {
		CompanyDAO companydao = new CompanyDAO();
		try {
			if(companydao.getCompanyDetailsById(company.getCompanyid()) != null)
					updateSharePrice(company, request, response);
			else
				saveCompany(company,request,response);
		} catch (ClassNotFoundException | SQLException  | ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveCompany(Company company, HttpServletRequest request, HttpServletResponse response) {
		
		CompanyDAO companydao = new CompanyDAO();
		try {
				companydao.save(company);			
				getCompanyDetails(request, response);
			
		} catch (ClassNotFoundException | SQLException | ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void updateSharePrice(Company company, HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, ServletException, IOException {
		CompanyDAO companyDao = new CompanyDAO ();
		companyDao.updateSharePrice(company);
		getCompanyDetails(request, response);
		
	}

	
	private void deleteCompany(int id, HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, IOException, ServletException {
		CompanyDAO companyDao = new CompanyDAO();
		companyDao.deleteCompany(id);
		getCompanyDetails( request, response);
	}

}
