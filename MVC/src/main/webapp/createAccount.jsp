<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create Account</title>
</head>
<body>

<h2>Create Account</h2>
<hr>

<%
String message = (String)session.getAttribute("message");
if(message== null)
	message = " ";
%>
<div style="color:red;font-size:26px">	<%= message %> </div>
	


<form action="ControllerServlet" method="get">
<table>
	<tr>
	<td> <label>FirstName</label>
	<td> <input type ="text" name="fname" required/></td>
	</tr>
	
	<tr>
	<td> <label>LastName</label>
	<td> <input type ="text" name="lname" required/></td>
	</tr>
	
	<tr>
	<td> <label>DOB</label>
	<td> <input type ="date" name="dob" required/></td>
	</tr>


	<tr>
	<td> <label>Gender</label></td>
	<td> <input type ="radio" name="gender" value="male"/>male</td>
	<td> <input type ="radio" name="gender" value="female"/>female</td>
	<td> <input type ="radio" name="gender" value="others"/>others</td>
	</tr>
	
	<tr>
	<td> <label>Email-ID</label>
	<td> <input type ="email" name="email" required/></td>
	</tr>
	
	<tr>
	<td> <label>Password</label>
	<td> <input type ="password" name="psswd" required/></td>
	</tr>
	
	<tr>
	<td>
		<input type ="submit" value="CreateAccount" name="create"/>
		<input type="reset" value="Reset" name="reset"/>
	</td>
	</tr>
</table>

</form>
</body>
</html>