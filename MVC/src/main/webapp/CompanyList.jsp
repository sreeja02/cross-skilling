<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Company Data</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
	crossorigin="anonymous">
	
</script>

<style>
.nav-link {
	text-decoration: none;
	color: white;
	margin-right: 13%;
}

p {
	font-size: 24px;
	font-weight: bold;
}

td {
	font-size: 20px;
}

img {
	width: 70px;
}

a {
	text-decoration: none;
}

a:hover {
	background-color: white;
}

#right {
	float: right;
}

body {
	margin-top: 50px;
}

#leading {
	background: linear-gradient(to top, #33ccff 0%, #ff99cc 100%);
	font-style: arial;
	color: white;
}
</style>

</head>
<body>

<div class="container">

	<div style="margin-left:85%">
	<a class="btn btn-dark"  href="Login.jsp">Logout</a>
	</div>

	
	
	<p>Company Share List</p>
		<h4><a href="ControllerServlet?id=0">Enlist new Company</a></h4>
	<table class="table">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Share Price</th>
			<th> Action </th>
		</tr>


		<c:forEach var="companyDetails" items="${requestScope.companyList}">
			<tr>
				<td><c:out value="${companyDetails. getCompanyid() }" /></td>
				<td><c:out value="${companyDetails. getCompanyName()} " /></td>

				<td><c:out value="${companyDetails. getShareprice()}" /></td>
	
				<td>
				<a class="btn btn-small btn-dark" href="ControllerServlet?id=${companyDetails. getCompanyid()  }"> update SharePrice</a> 
				
				<a class="btn btn-small btn-dark" href="ControllerServlet?deleteid=${companyDetails. getCompanyid()  }"> Delete</a> 
				</td>
			</tr>
		</c:forEach>

	</table>
	
</div>


</body>
</html>