<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Company Data</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
	crossorigin="anonymous">
	
</script>

<style>

p {
	font-size: 24px;
	font-weight: bold;
}

td {
	font-size: 20px;
}

img {
	width: 70px;
}

a {
	text-decoration: none;
}

a:hover {
	background-color: white;
}

#right {
	float: right;
}

body {
	margin-top: 50px;
}

#leading {
	background: linear-gradient(to top, #33ccff 0%, #ff99cc 100%);
	font-style: arial;
	color: white;
}
</style>

</head>
<body>

<div class="container">


	<div style="margin-left:85%">
	<a class="btn btn-dark"  href="Login.jsp">Logout</a>
	</div>

	
	
	<p>Company Form</p>
	<c:set var ="company" value="${sessionScope.Company }"/>
	<form action="ControllerServlet" method="get">
	<table class="table table-borderless">



	<tr>
	<td> <label>ID</label>
	<td> <input type ="text" name="companyID" value="${company.getCompanyid() }" /></td>
	</tr>
	
	<tr>
	<td> <label>Company Name</label>
	<td> <input type ="text" name="companyName" value="${company.getCompanyName() }" /></td>
	</tr>
	
	<tr>
	<td> <label>Share Price</label>
	<td> <input type ="text" name="shareprice" value="${company.getShareprice() }"/></td>
	</tr>
	<tr>
	<td>
		<input type ="submit" value="submit" name="submit"/>
		<input type="reset" value="Reset" name="reset"/>
	</td>
	</tr>
</table>
</form>

<a href ="ControllerServlet?list=shareList" class ="btn btn-dark" >Back to Company Share List</a>
</div>
</body>
</html>