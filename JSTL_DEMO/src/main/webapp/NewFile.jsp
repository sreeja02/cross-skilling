<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Demo</title>
</head>
<body>

<% out.print(new Date()); %>
<br>
<%
	//String s = "Hello";
	//int number = 10;
	
	
%>
<br>
<p>List Items

<c:set var="l1" value="[24,25,26]"></c:set>
<c:out value="${l1}"></c:out>
<c:forEach items="${l1}" var="n">
	<c:out value="${n}"></c:out>
</c:forEach> 
</p>
<br>
<c:set var= "s" value="JSTL"></c:set> <br>
<c:out value="${s}"></c:out>

<c:set var="number" value="10" scope="session"></c:set><br>
<c:out value="Addition = ${number+10}"></c:out>

<c:out value="Hello World"></c:out>

<c:if test="${number % 2 == 0}">
	<c:out value="Even Number"></c:out>
</c:if>
</body>
</html>